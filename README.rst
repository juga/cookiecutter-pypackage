======================
Cookiecutter PyPackage
======================

.. image:: https://pyup.io/repos/github/audreyfeldroy/cookiecutter-pypackage/shield.svg
    :target: https://pyup.io/repos/github/audreyfeldroy/cookiecutter-pypackage/
    :alt: Updates

.. image:: https://travis-ci.org/audreyfeldroy/cookiecutter-pypackage.svg?branch=master
    :target: https://travis-ci.org/github/audreyfeldroy/cookiecutter-pypackage
    :alt: Build Status

.. image:: https://readthedocs.org/projects/cookiecutter-pypackage/badge/?version=latest
    :target: https://cookiecutter-pypackage.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

Cookiecutter_ template for a Python package.

* GitHub repo: https://github.com/audreyfeldroy/cookiecutter-pypackage/
* Documentation: https://cookiecutter-pypackage.readthedocs.io/
* Free software: BSD license

Features
--------

* Testing setup with ``unittest`` and ``python setup.py test`` or ``pytest``
* Travis-CI_: Ready for Travis Continuous Integration testing
* Tox_ testing: Setup to easily test for Python 3.6, 3.7, 3.8, 3.9
* Sphinx_ docs: Documentation ready for generation with, for example, `Read the Docs`_
* Command line interface using Click (optional)
* Versioneer_: like a rocketeer, but for versions!
* pre-commit_ : It is a multi-language package manager for pre-commit hooks
* gitchangelog_: Use your commit log to make beautifull and configurable changelog file
* bandit_: a tool designed to find common security issues in Python code
* safety_: Use it to check your local virtual environment, your requirement
  files, or any input from stdin for dependencies with security issues.

.. _Cookiecutter: https://github.com/cookiecutter/cookiecutter

Build Status
-------------

Linux:

.. image:: https://img.shields.io/travis/audreyfeldroy/cookiecutter-pypackage.svg
    :target: https://travis-ci.org/audreyfeldroy/cookiecutter-pypackage
    :alt: Linux build status on Travis CI

Windows:

.. image:: https://ci.appveyor.com/api/projects/status/github/audreyr/cookiecutter-pypackage?branch=master&svg=true
    :target: https://ci.appveyor.com/project/audreyr/cookiecutter-pypackage/branch/master
    :alt: Windows build status on Appveyor

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    apt install cookiecutter virtualenvwrapper

(Optional) Customize this repo ``cookiecutter.json``.

Generate a Python package project::

    cookiecutter <this repo>

Then finish initializing it::
    cd <your new project>
    git init
    # It's recommended to create a virtualenv:
    mkvirtualenv <name your virtualenv>
    # Only in case versioneer is not included here:
    # pip install versioneer
    # versioneer install
    # Tor run the tests
    pip install -e .[test]
    tox
    # To install the commit hooks
    pip install -e .[dev]
    pre-commit install
    # Try running the hook before the commit
    pre-commit run --all-files
    # Commit the modified files
    git add .
    git commit -m "Initial import"

To temporally disable the pre-commit hooks::

    git commit --no-verify

.. _Travis-CI: http://travis-ci.org/
.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _Read the Docs: https://readthedocs.io/
.. _`pyup.io`: https://pyup.io/
.. _bump2version: https://github.com/c4urself/bump2version
.. _Punch: https://github.com/lgiordani/punch
.. _Poetry: https://python-poetry.org/
.. _PyPi: https://pypi.python.org/pypi
.. _Versioneer: https://github.com/python-versioneer/python-versioneer
.. _pre-commit: https://pre-commit.com
.. _gitchangelog: https://github.com/vaab/gitchangelog
.. _bandit: https://bandit.readthedocs.io
-- _safety: https://pyup.io/safety/

.. _`Nekroze/cookiecutter-pypackage`: https://github.com/Nekroze/cookiecutter-pypackage
.. _`tony/cookiecutter-pypackage-pythonic`: https://github.com/tony/cookiecutter-pypackage-pythonic
.. _`ardydedase/cookiecutter-pypackage`: https://github.com/ardydedase/cookiecutter-pypackage
.. _`lgiordani/cookiecutter-pypackage`: https://github.com/lgiordani/cookiecutter-pypackage
.. _`briggySmalls/cookiecutter-pypackage`: https://github.com/briggySmalls/cookiecutter-pypackage
.. _github comparison view: https://github.com/tony/cookiecutter-pypackage-pythonic/compare/audreyr:master...master
.. _`network`: https://github.com/audreyr/cookiecutter-pypackage/network
.. _`family tree`: https://github.com/audreyr/cookiecutter-pypackage/network/members
