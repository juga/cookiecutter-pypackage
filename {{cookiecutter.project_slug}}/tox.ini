[tox]
skip_missing_interpreters = True
envlist =
    py{36, 37, 38, 39, nightly}
    isort,
    black,
    lint
    doc,
    stats,
    bandit

[testenv]
usedevelop = true
deps = .[test]
commands =
    ; Use python -m to use the binary from the virtualenv if it is also
    ; installed in the system. Do not use {envbindir} since it would require
    ; to install it in tox virtualenv
    python -m coverage run --append --module pytest

[testenv:isort]
skip_install = True
deps = isort
commands =
  # Not using `python -m ` here because it seems then the settings from
  # setup.cfg are ignored
  isort --check-only --diff {{ cookiecutter.project_slug }} tests

[testenv:black]
skip_install = True
deps = black
commands =
  black --check --diff {{ cookiecutter.project_slug }} tests

[testenv:lint]
skip_install = True
deps = flake8
commands =
  flake8 {{ cookiecutter.project_slug }} tests

[testenv:clean]
skip_install = True
deps = coverage
command = coverage erase

[testenv:stats]
skip_install = True
deps = coverage
commands=
    # nothing to combine while not using several python versions
    ; python -m coverage combine
    python -m coverage report
    python -m coverage html

[testenv:doc]
deps = .[doc]
whitelist_externals = make
changedir = {toxinidir}/docs
commands =
    make html
    # this requires build the pdf images
    # make latexpdf
    ; make man

[testenv:bandit]
deps = bandit
# Add --exit-zero to exit successfully even if results are found
commands = bandit -r --exclude {{ cookiecutter.project_slug }}/_version.py {{ cookiecutter.project_slug }}/

# Test will pass anyway, just show the results.
# Unless a requirements.txt is given, it will only check dependencies in the
# virtualenv, ie. no system packages.
[testenv:safety]
deps = safety
commands = safety check --full-report
